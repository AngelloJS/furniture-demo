export const environment = {
  production: false,
  name: 'dev',
  apiUrl: 'https://laravelint.alisedainmobiliaria.com/',
  baseLocal: 'api/',
};
