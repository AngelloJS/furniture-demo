export const environment = {
  production: true,
  name: 'prod',
  apiUrl: 'https://laravelint.alisedainmobiliaria.com/',
  baseLocal: 'api/',
};

