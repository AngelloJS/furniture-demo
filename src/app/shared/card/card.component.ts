import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnDestroy {
  @Input() element: any;
  @Input() className: string = 'default';
  @Input() price: string = '';
  @Input() lastPrice: string = '';
  @Input() metaDescription: string = '';
  @Input() description: string = '';
  @Input() city: string = '';
  @Input() imagePath: string = '';
  @Input() area: number = 0;
  @Input() bedrooms: number = 0;
  @Input() bathrooms: number = 0;
  @Output() sendData: EventEmitter<any> = new EventEmitter<any>();

  ngOnDestroy(): void {
    this.sendData.unsubscribe();
  }

  sendReference() {
    this.sendData.emit(this.element);
  }
}
