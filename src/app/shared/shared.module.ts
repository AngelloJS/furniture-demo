import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
import { HttpClientModule } from '@angular/common/http';

import { CardModule } from 'primeng/card';

@NgModule({
  declarations: [
    CardComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    CardModule
  ],
  exports: [
    CardComponent,
    CardModule
  ]
})
export class SharedModule { }
