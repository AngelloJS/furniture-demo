import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { FurnitureService } from 'src/app/core/services/furniture.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ListComponent implements OnInit {
  elements: any[] = [];
  private readonly _unsubscribe$: Subject<void> = new Subject();

  constructor(private _furnitureService: FurnitureService) {}

  ngOnInit(): void {
    this.getAllFurnitures();
  }

  getAllFurnitures() {
    this._furnitureService
      .getFurnitures()
      .pipe(takeUntil(this._unsubscribe$))
      .subscribe({
        next: (response) => {
          if (response) {
            console.log(response);
            this.elements = response.data;
            // this.elements = this.elements.map(
            //   ({
            //     Bathrooms,
            //     Bedrooms,
            //     Precio,
            //     PrecioAnterior,
            //     Metadescription,
            //     Ciudad,
            //     UsableArea,
            //     imagenes,
            //     ...rest
            //   }) => rest
            // );
            console.log(this.elements);
          }
        },
        error: (error) => {
          console.log(error);
        },
      });
  }

  openDetail(event: any) {
    this._furnitureService.updatedDataSelection(event);
  }
}
