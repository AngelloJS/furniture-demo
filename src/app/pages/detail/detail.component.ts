import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FurnitureService } from 'src/app/core/services/furniture.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DetailComponent implements OnInit, OnDestroy {
  private subscription: Subscription | undefined;
  detailRequest: any;
  detailData: any;

  constructor(private _furnitureService: FurnitureService, private _router: Router) {}

  ngOnInit(): void {
    this.subscription = this._furnitureService.data.subscribe((data: any) => {
      if (data) {
        console.log(data);
        this.detailRequest = data;
      }
    });
    this.getDetails();
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  getDetails() {
    if (this.detailRequest !== undefined) {
      const { provinciaUrl, ciudadUrl, id } = this.detailRequest;
      this._furnitureService
      .getDetailOfFurniture(provinciaUrl, ciudadUrl, id)
      .subscribe({
        next: (data) => {
          if (data) {
            console.log(data);
            this.detailData = data;
          }
        },
        error: (error) => {
          console.log('error:', error);
        },
      });
    } else {
      this.backToCurrentPage();
    }
  }

  backToCurrentPage() {
    this._router.navigate(['']);
  }
}
