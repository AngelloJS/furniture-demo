import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, map } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class FurnitureService {
  private dataSource = new BehaviorSubject<any>(null);
  data = this.dataSource.asObservable();

  constructor(private http: HttpClient) {}

  updatedDataSelection(data: any){
    this.dataSource.next(data);
  }

  getFurnitures(): Observable<any> {
    const url = environment.apiUrl + `new-search?tipo=10`;
    return this.http.get<any>(url).pipe(map((res) => res));
  }

  getDetailOfFurniture(
    provinciaUrl: string,
    poblacionUrl: string,
    reference: string
  ): Observable<any> {
    const url =
      environment.apiUrl +
      `get-property/10/${provinciaUrl}/${poblacionUrl}/${reference}`;
    return this.http.get<any>(url).pipe(map((res) => res));
  }
}

// https://laravelint.alisedainmobiliaria.com/api/get-property/10/castellon/peniscolapeniscola/CAN0000188391
